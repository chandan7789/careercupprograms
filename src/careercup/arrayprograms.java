package com.career.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

public class ArrayTests {

	@Test
	public void pushZeros() {

		int[] array = { 1, 2, 0, 4, 0, 5, 1, 0 };

		int count = 0;

		for (int i = 0; i < array.length; i++) {

			if (array[i] != 0) {
				array[count++] = array[i];
			}
		}

		while (count < array.length) {
			array[count++] = 0;
		}

		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

	@Test
	public void sortArrary() {

		int[] array = { 1, 2, 0, 4, 0, 5, 1, 0 };

		Arrays.sort(array);

		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

	@Test
	public void sortArraryBubbleSort() {

		int[] a = { 1, 2, 0, 4, 0, 5, 1, 0 };

		for (int i = 0; i < a.length - 1; i++) {
			for (int j = 0; j < a.length - 1 - i; j++) {

				if (a[j] > a[j + 1]) {
					int tmp = a[j];
					a[j] = a[j + 1];
					a[j + 1] = tmp;
				}
			}
		}

		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

	@Test
	public void rotateArray() {

		int[] array = { 1, 2, 0, 4, 0, 5, 1, 0 };
		int[] tmp = new int[array.length];

		int n = 2;
		int k = 0;

		// first store n to len-1 in tmp array
		for (int i = n; i < array.length; i++) {
			tmp[k] = array[i];
			k++;
		}

		// now store initial n elements at the last
		for (int i = 0; i < n; i++) {
			tmp[k] = array[i];
			k++;
		}

		System.out.println(tmp);
	}

	@Test
	public void longestSubStringWithoutRepeatation() {

	}

	@Test
	public void groupAnagrams() {
		String[] words = { "aba", "baa", "cat", "bat", "tac" };
		System.out.println(groupAnagrams(words));
	}

	public List<List<String>> groupAnagrams(String[] strs) {
		if (strs == null || strs.length == 0)
			return new ArrayList<>();
		Map<String, List<String>> map = new HashMap<>();
		for (String s : strs) {
			char[] ca = s.toCharArray();
			Arrays.sort(ca);
			String keyStr = String.valueOf(ca);
			if (!map.containsKey(keyStr))
				map.put(keyStr, new ArrayList<>());
			map.get(keyStr).add(s);
		}
		return new ArrayList<>(map.values());
	}

	@Test
	public void maxSubArray() {
		int[] array = { -2, 4, 4, 0, -2 };

		int sum = array[0], max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (sum < 0) {
				sum = array[i];
			} else {
				sum += array[i];
			}
			max = Math.max(max, sum);
		}
		System.out.println(max);
	}
}