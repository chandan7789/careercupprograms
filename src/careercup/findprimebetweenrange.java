package careercup;

import java.util.Scanner;
public class findprimebetweenrange
{
    public static void main(String args[])
    {
         int n, l, r, check = 0, i, j;
         Scanner s = new Scanner(System.in);
         System.out.println ("Enter the lower limit :"); 
         l = s.nextInt();
         System.out.println ("Enter the upper limit :"); 
         r = s.nextInt();
         System.out.println ("The prime numbers in between the entered limits are :");
         for(i = l; i <= r; i++)
         {
             for( j = 2; j < i; j++)
             {
                 if(i % j == 0)
                 {
                     check = 0;
                     break;
                 }
                 else
                 {
                     check = 1;
                 }
             }
             if(check == 1)
             {
                 System.out.println(i);
             }
         }
    }
}