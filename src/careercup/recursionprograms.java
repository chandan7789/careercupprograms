package com.career.tests;

import org.testng.annotations.Test;

public class RecursionTests {

	@Test
	public void factorial() {

		int n = 5;
		System.out.println(factorialRecursion(n));
	}

	private int factorialRecursion(int n) {
		if (n == 0 || n == 1) {
			return 1;
		} else {

			return n * factorialRecursion(n - 1);
		}

	}
}
