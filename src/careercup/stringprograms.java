package com.career.tests;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.testng.annotations.Test;

public class StringTests {
	@Test
	public void removeDupFromString() {

		String s = "chandan";

		char[] array = s.toCharArray();

		// to maintain insertion order linkedhashset

		LinkedHashSet<Character> result = new LinkedHashSet<>();

		for (Character item : array) {

			result.add(item);
		}

		System.out.println(result);

	}

	@Test
	public void findCharCount() {

		String s = "chandan";

		char[] array = s.toCharArray();
		LinkedHashMap<Character, Integer> result = new LinkedHashMap<>();

		for (Character item : array) {

			if (result.containsKey(item)) {
				result.put(item, result.get(item) + 1);
			} else {
				result.put(item, 1);

			}
		}

		System.out.println(result);

	}

	@Test
	public void reverseString() {

		String s = "chandan";
		String result = new String();

		char[] array = s.toCharArray();

		for (int i = s.length() - 1; i > -1; i--) {
			result = result + array[i];
		}

		System.out.println(result);

	}

	@Test
	public void palidrome() {

		String s = "aba";
		String result = new String();

		char[] array = s.toCharArray();

		for (int i = s.length() - 1; i > -1; i--) {
			result = result + array[i];
		}

		if (result.equals(s)) {
			System.out.println("palidrome");
		} else {
			System.out.println("not palidrome");
		}
	}

	@Test
	public void anagram() {

		String s1 = "listen";
		String s2 = "silent";
		int sum1 = 0;
		int sum2 = 0;
		char[] array1 = s1.toCharArray();
		char[] array2 = s2.toCharArray();

		for (Character item : array1) {
			sum1 = sum1 + (int) item;
		}
		for (Character item2 : array2) {
			sum2 = sum2 + (int) item2;
		}

		if (sum1 == sum2) {
			System.out.println("String is anagram");
		} else {
			System.out.println("String is not anagram");
		}
	}
}
