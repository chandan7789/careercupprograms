package careercup;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class charcountusinghashmap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 String str = "chandan";

	        char[] char_array = str.toCharArray();

	        System.out.println("The Given String is : " + str);

	    HashMap<Character, Integer> charCounter = new HashMap<Character, Integer>();

	        for (char i : char_array) {

	    charCounter.put(i,charCounter.get(i) == null ? 1 : charCounter.get(i) + 1);

	        }

	    for (Character key : charCounter.keySet()) {
	  System.out.println("occurrence of '" + key + "' is  "+ charCounter.get(key));
	        }


	  //iteration using foreach and keyset
	    for (Character key2 : charCounter.keySet()) {
	      Integer s= charCounter.get(key2);
	      

	      System.out.println(key2);
	      System.out.println(s);
	    }
	  //iteration using foreach and entryset
	    for (Map.Entry<Character, Integer> pair:charCounter.entrySet())
	    {
	     System.out.println("key is="+pair.getKey());

	     System.out.println("Value is="+pair.getValue());
	      
	      
	    }
	}
	
	

}
