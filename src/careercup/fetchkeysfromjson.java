package careercup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.akiban.sql.parser.ParseException;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

public class fetchkeysfromjson {

	public static void main(String[] args) throws ParseException, net.minidev.json.parser.ParseException {
	    String str = "{ \"empId\":\"1\", \"name\":\"Alex\", \"role\":\"president\", \"phone\":\"123\", \"address\": { \"street\":\"xyz\", \"city\":\"hyd\", \"pincode\":400123 }}";
	    @SuppressWarnings("deprecation")
		JSONObject obj = (JSONObject) new JSONParser().parse(str);
	    List<String> keysList = new ArrayList<>();
	    recLevels(keysList, obj, "");
	    System.out.println(keysList);
	}

	public static void recLevels(List<String> keysList, JSONObject obj, String prefix) {
	    Set<String> keys = (Set<String>) obj.keySet();
	    for (String key : keys) {
	        keysList.add(key);
	        if (obj.get(key) instanceof JSONObject) {
	            recLevels(keysList, (JSONObject) obj.get(key), prefix + (prefix.isEmpty() ? "" : ".") + key);
	        }
	    }
	}

}
